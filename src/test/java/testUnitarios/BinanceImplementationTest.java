package testUnitarios;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import asset.Asset;
import operation.OperationType;
import operation.Order;
import plugins.BinanceImplementation;

public class BinanceImplementationTest {
	private BinanceImplementation binance;
	double quantity;
	double marketPrice;
	Asset asset;
	OperationType operationType;
	
    
	@Before
	public void init() {
		binance = new BinanceImplementation();
		quantity = 5;
		marketPrice = 100;
		asset = new Asset("BITCOIN","BTC");
	}
    
	@Test
	public void testPlaceOrder() {
		Order order = new Order(100, new Date(), 100, asset, OperationType.BUY);
		assertTrue(binance.placeOrder(order).length()>0);
	}
	
	@Test
    public void testGetPrice()  
    {
		assertTrue(binance.getPrice("BTC")>0); 
    }
    
    @Test
    public void testGetName()
    {
    	assertTrue(binance.getName() == "BINANCE");
    }
	
}
