package testUnitarios;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import bitrader.config.ConfigContent;

import static org.junit.Assert.assertTrue;
public class Configuration {
	private JsonObject jsonObject;
	ConfigContent configContent;
	JsonArray rules;
	
	@Before
	public void init() {
		jsonObject = new JsonObject();
		rules = new JsonArray(1);
		String rule = "{\"name\":\"BITCOIN\", \"symbol\":\"BTC\",\"quantity\":1,\"type\":\"BUY\","+"\"percentage\":1}";
		JsonParser parser = new JsonParser();
		rules.add(parser.parse(rule));
	}
	
	@Test
	public void testParseOk(){
		ConfigContent configContent = createOk();
		assertTrue(configContent.create());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testParseFailKeyFrecuencyNotExists(){
		jsonObject.add("amount",new JsonPrimitive(1));
		jsonObject.add("rules", rules);
		configContent = new ConfigContent(jsonObject);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testParseFailKeyRulesNotExists(){
		jsonObject.add("frecuency", new JsonPrimitive(1));
		jsonObject.add("amount",new JsonPrimitive(1));
		configContent = new ConfigContent(jsonObject);
	}
	
	@Test 
	public void testParseRulesNull(){
		jsonObject.add("frecuency", new JsonPrimitive(1));
		jsonObject.add("amount",new JsonPrimitive(1));
		jsonObject.add("rules", new JsonArray());
		configContent = new ConfigContent(jsonObject);
		configContent.create();
		assertTrue(configContent.getNames().length==0);
		assertTrue(configContent.getSymbols().length==0);
		assertTrue(configContent.getTypes().length==0);
		assertTrue(configContent.getPercentages().length==0);
		assertTrue(configContent.getQuantities().length==0);
	}
	
	private ConfigContent createOk()
	{
		jsonObject.add("frecuency", new JsonPrimitive(1));
		jsonObject.add("amount", new JsonPrimitive(100));
		jsonObject.add("rules", rules);
		configContent = new ConfigContent(jsonObject);
		return configContent;
	}
	
	public void testGetValues()
	{
		ConfigContent configContent = createOk();
		double[] quantities = {1};
		String[] names = {"BITCOIN"};
		String[] symbols = {"BTC"};
		assertTrue(configContent.getAmount()==100);
		assertTrue(configContent.getFrecuency()==1);
		assertTrue(configContent.getQuantities() == quantities);
		assertTrue(configContent.getNames() == names);
		assertTrue(configContent.getSymbols() == symbols);
		assertTrue(configContent.getPercentages() == quantities);
	}

}
