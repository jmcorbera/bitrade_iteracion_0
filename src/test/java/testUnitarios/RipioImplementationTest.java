package testUnitarios;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import asset.Asset;
import operation.OperationType;
import operation.Order;
import plugins.RipioImplementation;

public class RipioImplementationTest {
	private RipioImplementation ripio;
	double quantity;
	double marketPrice;
	Asset asset;
	OperationType operationType;
	
    
	@Before
	public void init() {
		ripio = new RipioImplementation();
		quantity = 5;
		marketPrice = 100;
		asset = new Asset("BITCOIN","BTC");
	}
    
	@Test
	public void testPlaceOrder() {
		Order order = new Order(100, new Date(), 100, asset, OperationType.BUY);
		assertTrue(ripio.placeOrder(order)==null);
	}
	
	@Test
    public void testGetPrice()  
    {
		assertTrue(ripio.getPrice("BTC")==0); 
    }
    
    @Test
    public void testGetName()
    {
    	assertTrue(ripio.getName() == "RIPIO");
    }
}
