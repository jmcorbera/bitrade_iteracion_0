package mock;

import market.Market;
import operation.Order;

public class MarketFake implements Market {

	@Override
	public double getPrice(String symbol) {
		return 100;
	}

	@Override
	public String getName() {
		return "BINANCE";
	}

	@Override
	public String placeOrder(Order order) {
		return "";
	}



}
