package bitrader.config;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ConfigContent{
	private JsonObject jObject;
	private double percentages[];
	private int frecuency;
	private double amount;
	private double quantity[];
	private String names[];
	private String symbols[];
	private String types[];

	public ConfigContent(JsonObject jObject) {
		if (!keysValid(jObject))
			throw new IllegalArgumentException();
		this.jObject = jObject;
	}

	public boolean create() {
		try {
			this.frecuency = jObject.get("frecuency").getAsInt();
			this.amount = jObject.get("amount").getAsDouble();
			JsonArray rules = jObject.getAsJsonArray("rules");
			int rulesSize = rules.size();
			this.percentages = new double[rulesSize];
			this.quantity = new double[rulesSize];
			this.names = new String[rulesSize];
			this.symbols = new String[rulesSize];
			this.types = new String[rulesSize];

			for (int i = 0; i < rulesSize; i++) {
				JsonObject object = (JsonObject) rules.get(i);
				this.percentages[i] = object.get("percentage").getAsDouble();
				this.quantity[i] = object.get("quantity").getAsDouble();
				this.names[i] = object.get("name").getAsString();
				this.symbols[i] = object.get("symbol").getAsString();
				this.types[i] = object.get("type").getAsString();
			}
			return true;
		} catch (Exception e) {
			throw e;
		}

	}

	public double[] getPercentages() {
		return this.percentages;
	}

	public double[] getQuantities() {
		return this.quantity;
	}

	public String[] getNames() {
		return this.names;
	}

	public String[] getSymbols() {
		return this.symbols;
	}

	public int getFrecuency() {
		return this.frecuency;
	}
	
	public double getAmount() {
		return amount;
	}

	public String[] getTypes() {
		return this.types;
	}
	
	private static boolean keysValid(JsonObject jObject) {
		if (jObject.get("rules") == null || jObject.get("frecuency") == null)
			return false;
		return true;
	}


}
