package bitrader;

import com.google.gson.JsonObject;

import bot.Bot;
import bot.BotSetupDTO;
import market.Market;
import market.MarketFinder;
import setup.SetupCreator;
import wallet.Wallet;

public class BiTrader {
	public static void main(String[] args) {
		try {
			JsonObject discoverJson = SetupCreator.getJsonObject(SetupCreator.discoverName);
		    JsonObject configJson = SetupCreator.getJsonObject(SetupCreator.configName);
			BotSetupDTO setup = SetupCreator.create(discoverJson, configJson);
			MarketFinder marketFinder = new MarketFinder();
			
			marketFinder.initialize(setup.getPath(), setup.getPackage());
			Market market = marketFinder.getMarkets().iterator().next();
			Wallet wallet = new Wallet();
			Bot bot = new Bot(wallet, setup.getRules(), setup.getLoopTime(), market);
			bot.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
