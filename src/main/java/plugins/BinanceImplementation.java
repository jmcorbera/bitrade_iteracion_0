package plugins;
import market.Market;
import operation.Order;
import java.util.Random;

public class BinanceImplementation implements Market {
	private final String name = "BINANCE";
	
	@Override
	public double getPrice(String symbol) {
		Random r = new Random();
		return r.nextDouble()*100000 +1;
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String placeOrder(Order order) {
		return generateCode();
	}

	private String generateCode() {
		String orderCode = "";
		for(int i = 0; i < 3; i++){
			orderCode += generateRandomLetter() + generateRandomNumber();
		}
		return orderCode;
	}


	private String generateRandomNumber() {
		Random r = new Random();
		return String.valueOf(r.nextInt(26));

	}

	private String generateRandomLetter() {
		Random r = new Random();
		return String.valueOf(r.nextInt(26)+ 'a');
	}
}
